<?php
require('../src/rssreader.php');

$format = "plain";
$order = "time";

if (php_sapi_name() == "cli") { // Running in cli
  $options = getopt("", ["format::", "order::"]);
  if (array_key_exists("format", $options)) {
    if (array_key_exists($options["format"], $RSSOptions["format"])) { // Check if valid format
      $format = $options["format"];
    } else {
      echo "Format: '".$options["format"]."' is not implemented, try 'plain, or 'html'\n";
      exit(1);
    }
  }
  if (array_key_exists("order", $options)) {
    if (array_key_exists($options["order"], $RSSOptions["order"])){ // Check if valid order
      $order = $options["order"];
    } else {
      echo "Order: '".$options["order"]."' is not implemented, try 'time' or 'title'\n";
      exit(1);
    }
  }
} elseif (isset($_REQUEST)) { // Running in web
  $format = "html";
  if (isset($_GET["order"])) {
    if (array_key_exists($_GET["order"], $RSSOptions["order"])) { // Check if valid order
      $order = $_GET["order"];
    }
  }
}

$reader = new RSSReader;
$res = $reader->ingest_source("https://rss.art19.com/apology-line");
if (!$res) {
  error_log("Could not get ingest source: https://rss.art19.com/apology-line, check internet connection and path");
}

$res = $reader->ingest_source("https://hacks.mozilla.org/feed");
if (!$res) {
  error_log("Could not get ingest source: https://hacks.mozilla.org/feed, check internet connection and path");
}

$reader->sort($order);

$output =  $reader->format($format);

// Checking if some error occurred in format, this should be unreachable since only valid options are accepted but you never know.
if ($output === "") {
  echo "An unknown error has occured, please try again later\n";
  error_log("An unknown error has occured, format output was \"\"");
} else {
  echo $output;
}
?>

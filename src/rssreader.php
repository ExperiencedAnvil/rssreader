<?php
/**
 * Toy implementation of a RSS reader.
 * @author Felix
 * @version 1.0.0
 */

/**
 * Valid options for format and order for use with array_key_exists()
 */
$RSSOptions = [
  "order" => [
    "time" => 1,
    "title" => 1,
  ],
  "format" => [
    "plain" => 1,
    "html" => 1,
  ]
];

/**
 * RSS reader class
 *
 * Ingest data by using ingest_source("path")
 * Sort it with sort("<method>") where method can be 'title' or 'time'(default)
 * Output the feed in text or html using format("<format>"), where <format> can be 'plain' or 'html'
 *
 */
class RSSReader {
  private array $sources = [];
  private array $feed = [];

  /**
   * Reads a RSS 2.0 feed from a url or filepath
   *
   * @param string $src_file path for the feed
   *
   * @return bool True on success, False on fail
   */
  public function ingest_source(string $src_file): bool {
    $xml = @simplexml_load_file($src_file);
    if ($xml === false) {
      return false;
    }
    foreach($xml->channel->item as $post) {
      array_push($this->feed, $post);
    }
    return true;
  }

  /**
   * Sorts feed in specified order, default reverse chronological order
   *
   * Current orders are 'time' - for reverse chronological order, or 'title' for alphabetical sort on the titles
   * @param string $method sort-mode to use, 'time'||'title'
   * 
   * @return bool True on success and False on fail or invalid option
   */
  public function sort(string $method="time"): bool {
    if ($method == "title") {
      usort($this->feed, ["RSSReader", "sort_title"]);
      return true;
    } elseif ($method == "time") {
      usort($this->feed, ["RSSReader", "sort_time"]);
      return true;
    }
    return false;
  }

  /**
   * Outputs feed in specified format
   *
   * Wrapper function for ease of use
   *
   * @param $format string Either 'plain' or 'html' for plain-text and html respectively
   *
   * @return string formatted feed output returns empty string on invalid format
   */
  public function format(string $format): string {
    switch($format) {
      case "plain":
        return $this->output_plain();
        break;
      case "html":
        return $this->output_html();
        break;
      default:
        return "";
        break;
    }
  }

  /**
   * Outputs feed as php-array for use in tests or other integrations
   *
   * @return array feed as php-array
   */
  public function output_array(): array {
    return $this->feed;
  }

  /**
   * Outputs feed as formatted plain text
   *
   * @return string feed as formatted plain text
   */
  private function output_plain(): string {
    $output = "";
    foreach($this->feed as $post) {
      $desc = strip_tags($post->description);
      $output .= $this->remove_timezone("$post->pubDate")."| ".$post->title."\n".$desc."\n".$post->link."\n\n\n";
    }
    return $output;
  }

  /**
   * Outputs feed as html
   *
   * @return string html file ready to serve
   */
  private function output_html(): string {
    $top = file_get_contents("../templates/index_top.html");
    if ($top === false ) {
      error_log("Could not find ../templates/index_top.html, please check that the project is set up correctly");
      exit(1);
    }
    $bot = file_get_contents("../templates/index_bot.html");
    if ($bot === false) {
      error_log("Could not find ../templates/index_bot.html, please check that the project is set up correctly");
      exit(1);
    }
    
    $output = "";
    foreach($this->feed as $post) {
      $output .= '<div class="row "><a style="color:inherit" target="_blank" rel="noopener noreferrer" href='.$post->link.'>';
      $output .= '<div class="col"><p>'.$this->remove_timezone($post->pubDate).'</p></div>';
      $output .= '<div class="col"><h6>'.$post->title.'</h6></div>';
      $output .= '<div class="col"><p>'.$post->description.'</p></div>';
      $output .= '</a></div>'; // row
    }
    return $top.$output.$bot;
  }

  /**
   * Remove timezone from RFC 822 date/time string
   * 
   * @param string $t RFC 822 time string
   *
   * @return string RFC 822 time string without timezone
   */
  private function remove_timezone(string $t): string {
      // Removes timezone (\-|\+)\d{4} from timestamp, assuming RFC 822 format
      return explode("+", explode("-",$t)[0])[0];
  }

  /**
   * Compare 2 RSS posts to see which title is alphabetically first
   *
   * This is just a strcasecmp on the title for use with usort on SimpleXMLElements
   *
   * @param SimpleXMLElement $a Post 1
   * @param SimpleXMLElement $b Post 2
   *
   * @return int See https://www.php.net/manual/en/function.strcasecmp.php
   */
  private function sort_title(SimpleXMLElement $a, SimpleXMLElement $b): int {
    return strcasecmp($a->title, $b->title);
  }

  /**
   * Sorting RSS posts by time in reverse chronological order for use with usort
   *
   * @param SimpleXMLElement $a Post 1
   * @param SimpleXMLElement $b Post 2
   *
   * @return int <0 if $a is more recent, 0 if they are posted at same time and >0 if $b is more recent
   */
  private function sort_time(SimpleXMLElement $a, SimpleXMLElement $b): int {
    $t1 = strtotime($a->pubDate);
    $t2 = strtotime($b->pubDate);

    if ($t1 < $t2) {
      return 1;
    } elseif ($t1 == $t2) {
      return 0;
    } else {
      return -1;
    }
  }

};
?>

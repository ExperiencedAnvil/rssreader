<?php
use PHPUnit\Framework\TestCase;
require 'rssreader.php';

final class RSSReaderTest extends TestCase {

  private RSSReader $reader;

  protected function setUp(){
    $this->reader = new RSSReader;
  }

  // Test data read
  public function test_ingest_source(): void {
    // Known bad filepath to test fail
    $res = $this->reader->ingest_source('../test_data/file_does_not_exist.xml');
    $this->assertFalse($res, "ingest_source incorrectly gives no error when reading ../test_data/file_does_not_exist.xml");
    $this->assertEmpty($this->reader->output_array(), "ingest_source ingested something non-existent from ../test_Data/file_does_not_exist.xml");

    $res = $this->reader->ingest_source('../test_data/sample_feed_mozilla.xml');
    $this->assertTrue($res, "Could not ingest source: ../test_data/sample_feed_mozilla.xml");

    // The test file has 20 entries
    $this->assertCount(20, $this->reader->output_array(), "count(this->feed) should be 20, got: ".count($this->reader->output_array()));
  }

  // Test sorting funcitons
  public function test_sort(): void {
    $this->reader->ingest_source('../test_data/sample_feed_mozilla.xml');
    $res = $this->reader->sort("title");
    $this->assertTrue($res, "Got an error while sorting array by title.");

    $arr = $this->reader->output_array();

    // Checking against the previous item for alphabetical order
    for($i = 1; $i < count($arr); $i++) {
      $this->assertTrue((strcasecmp($arr[$i-1]->title, $arr[$i]->title) < 0 ), "The posts are not in alphabetical order");
    }

    // Time
    $res = $this->reader->sort("time");
    $this->assertTrue($res, "usort with time method failed.");

    $arr = $this->reader->output_array();
    // Checking chronological order against previous item
    for ($i = 1; $i < count($arr); $i++) {
      $t1 = strtotime($arr[$i-1]->pubDate);
      $this->assertFalse($t1 === FALSE, "failed to convert pubDate to time");
      $t2 = strtotime($arr[$i]->pubDate);
      $this->assertFalse($t2 === FALSE, "failed to convert pubDate to time");

      $this->assertTrue(($t1 >= $t2), "the posts are not in reverse chronological order after time sort");

    }
  }

  
  // Regression test for html output, generate new files with test_data/test_gen.php
  public function test_output_plain(): void {
    $this->reader->ingest_source('../test_data/sample_feed_mozilla.xml');
    $control_file = file_get_contents('../test_data/plain_output');
    $this->assertFalse($control_file === false, "Error opening the control file, this is related to test-code and/or resources, not project");

    $this->assertEquals($control_file, $this->reader->format("plain"), "The output doesn't match the saved control file.");
  }

  // Regression test for html output, generate new files with test_data/test_gen.php
  public function test_output_html(): void {
    $this->reader->ingest_source('../test_data/sample_feed_mozilla.xml');
    $control_file = file_get_contents('../test_data/html_output');
    $this->assertFalse($control_file === false, "Error opening the control file, this is related to test-code and or resources, not project.");

    $this->assertEquals($control_file, $this->reader->format("html"));
  }
}

?>

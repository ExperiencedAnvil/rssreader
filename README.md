## Rss reader implementation 
 
Entrypoint is public/index.php and the CWD needs to be public/ 
 
To run on linux CLI: 
```
cd public  
php index.php --format=<plain|html> --order=<time|title> 
```
 
To run on php dev server: 
```
cd public/ 
php -S 127.0.0.1:8000 
```
then browse to http://127.0.0.1:8000 
 
 
To run tests with phpunit: 
``` 
cd src/ 
phpunit rssreader_test.php 
```

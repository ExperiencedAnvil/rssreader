<?php
include "../src/rssreader.php";

$reader = new RSSReader;

$reader->ingest_source('sample_feed_mozilla.xml');
$plain = $reader->format("plain");
$html = $reader->format("html");

file_put_contents("html_output", $html);
file_put_contents("plain_output", $plain);
?>

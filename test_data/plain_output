Wed, 20 Oct 2021 14:35:47 | Hacks Decoded: Thomas Park, Founder of Codepip
Welcome to our Hacks: Decoded Interview series! We spoke with Thomas Park over email about coding, his favourite apps and his past life at Mozilla. Thomas is the founder of Codepip, a platform he created for coding games that helps people learn HTML, CSS, JavaScript, etc. The most popular game is Flexbox Froggy.
The post Hacks Decoded: Thomas Park, Founder of Codepip appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/10/hacks-decoded-thomas-park-founder-of-codepip/


Fri, 08 Oct 2021 16:45:37 | Lots to see in Firefox 93!
Firefox 93 comes with lots of lovely updates including AVIF image format support, filling of XFA-based forms in its PDF viewer and protection against insecure downloads by blocking downloads relying on insecure connections.
The post Lots to see in Firefox 93! appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/10/lots-to-see-in-firefox-93/


Thu, 07 Oct 2021 15:13:02 | Implementing form filling and accessibility in the Firefox PDF viewer
Last year, during lockdown, many discovered the importance of PDF forms when having to deal remotely with administrations and large organizations like banks. Firefox supported displaying PDF forms, but it didn’t support filling them: users had to print them, fill them by hand, and scan them back to digital form. We decided it was time to reinvest in the PDF viewer (PDF.js) and support filling PDF forms within Firefox to make our users' lives easier.
The post Implementing form filling and accessibility in the Firefox PDF viewer appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/10/implementing-form-filling-and-accessibility-in-the-firefox-pdf-viewer/


Wed, 06 Oct 2021 14:55:48 | Control your data for good with Rally
In a world where data and AI are reshaping society, people currently have no tangible way to put their data to work for the causes they believe in. To address this, we built the Rally platform, a first-of-its-kind tool that enables you to contribute your data to specific studies and exercise consent at a granular level. Mozilla Rally puts you in control of your data while building a better Internet and a better society. 
The post Control your data for good with Rally appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/10/control-your-data-for-good-with-rally/


Tue, 05 Oct 2021 16:52:07 | Tab Unloading in Firefox 93
Starting with Firefox 93, Firefox will monitor available system memory and, should it ever become so critically low that a crash is imminent, Firefox will respond by unloading memory-heavy but not actively used tabs. This feature is currently enabled on Windows and will be deployed later for macOS and Linux as well.
The post Tab Unloading in Firefox 93 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/10/tab-unloading-in-firefox-93/


Fri, 01 Oct 2021 12:46:37 | MDN Web Docs at Write the Docs Prague 2021
The MDN Web Docs team is pleased to sponsor Write the Docs Prague 2021, which is being held remotely this year. We’re excited to join hundreds of documentarians to learn more about collaborating with writers, developers, and readers to make better documentation. We plan to take part in all that the conference has to offer, including the Writing Day, Job Fair, and the virtual hallway track.
The post MDN Web Docs at Write the Docs Prague 2021 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/10/mdn-web-docs-at-write-the-docs-prague-2021/


Wed, 08 Sep 2021 15:17:15 | Time for a review of Firefox 92
Release time comes around so quickly! This month we have quite a few CSS updates, along with the new Object.hasOwn() static method for JavaScript.
The post Time for a review of Firefox 92 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/09/time-for-a-review-of-firefox-92/


Wed, 18 Aug 2021 15:05:06 | Spring cleaning MDN: Part 2
Last month we removed a bunch of content from MDN. MDN is 16 years old (and yes it can drink in some countries), all that time ago it was a great place for all of Mozilla to document all of their things. As MDN evolved and the web reference became our core content, other areas became less relevant to the overall site. We have ~11k active pages on MDN, so keeping them up to date is a big task and we feel our focus should be there.
The post Spring cleaning MDN: Part 2 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/08/spring-cleaning-mdn-part-2/


Tue, 10 Aug 2021 15:04:27 | Hopping on Firefox 91
August is already here, which means so is Firefox 91! For developers, Firefox 91 supports the Visual Viewport API and Intl.DateTimeFormat object additions.
The post Hopping on Firefox 91 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/08/hopping-on-firefox-91/


Tue, 03 Aug 2021 15:49:30 | How MDN’s autocomplete search works
Last month, Gregor Weber and Peter Bengtsson added an autocomplete search to MDN Web Docs, that allows you to quickly jump straight to the document you're looking for by typing parts of the document title. This is the story about how that's implemented. 
The post How MDN&#8217;s autocomplete search works appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/08/mdns-autocomplete-search/


Tue, 20 Jul 2021 16:00:48 | Spring Cleaning MDN: Part 1
As we’re all aware by now, we made some big platform changes at the end of 2020. Whilst the big move has happened, it’s given us a great opportunity to clear out the cupboards and closets. 
The post Spring Cleaning MDN: Part 1 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/07/spring-cleaning-mdn-part-1/


Tue, 13 Jul 2021 15:02:10 | Getting lively with Firefox 90
As the summer rolls around for those of us in the northern hemisphere, temperatures are high and unwinding with a cool ice tea is high on the agenda. Isn't it lucky then that Background Update is here for Windows, which means Firefox can update even if it's not running. We can just sit back and relax!
Also this release we see a few nice JavaScript additions, including private fields and methods for classes, and the at() method for Array, String and TypedArray global objects.
This blog post just provides a set of highlights.
The post Getting lively with Firefox 90 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/07/getting-lively-with-firefox-90/


Tue, 08 Jun 2021 15:26:19 | Implementing Private Fields for JavaScript
When implementing a language feature for JavaScript, an implementer must make decisions about how the language in the specification maps to the implementation. Private fields is an example of where the specification language and implementation reality diverge, at least in SpiderMonkey– the JavaScript engine which powers Firefox. To understand more, I’ll explain what private fields are, a couple of models for thinking about them, and explain why our implementation diverges from the specification language.
The post Implementing Private Fields for JavaScript appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/06/implementing-private-fields-for-javascript/


Tue, 01 Jun 2021 15:19:01 | Looking fine with Firefox 89
Firefox 89 has smartened up and brings with it a slimmed-down, slightly more minimalist interface.
Along with this new look, we get some great styling features including a force-colours feature for media queries and better control over how fonts are displayed. The long-awaited top-level await keyword for JavaScript modules is now enabled, as well as the PerformanceEventTiming interface, which is another addition to the performance suite of APIs: 89 really has been working out!
The post Looking fine with Firefox 89 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/06/looking-fine-with-firefox-89/


Wed, 19 May 2021 14:28:53 | Improving Firefox stability on Linux
Roughly a year ago at Mozilla we started an effort to improve Firefox stability on Linux. This effort quickly became an example of good synergies between FOSS projects.
The post Improving Firefox stability on Linux appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/05/improving-firefox-stability-on-linux/


Tue, 18 May 2021 15:45:15 | Introducing Firefox’s new Site Isolation Security Architecture
Like any web browser, Firefox loads code from untrusted and potentially hostile websites and runs it on your computer. To protect you against new types of attacks from malicious sites and to meet the security principles of Mozilla, we set out to redesign Firefox on desktop.
The post Introducing Firefox&#8217;s new Site Isolation Security Architecture appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/05/introducing-firefox-new-site-isolation-security-architecture/


Thu, 22 Apr 2021 15:17:11 | Pyodide Spin Out and 0.17 Release
We are happy to announce that Pyodide has become an independent and community-driven project. We are also pleased to announce the 0.17 release for Pyodide with many new features and improvements. Pyodide consists of the CPython 3.8 interpreter compiled to WebAssembly which allows Python to run in the browser.
The post Pyodide Spin Out and 0.17 Release appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/04/pyodide-spin-out-and-0-17-release/


Mon, 19 Apr 2021 15:18:52 | Never too late for Firefox 88
April is upon us, and we have a most timely release for you — Firefox 88. In this release you will find a bunch of nice CSS additions including :user-valid and :user-invalid support and image-set() support, support for regular expression match indices, removal of FTP protocol support for enhanced security, and more! This blog post [&#8230;]
The post Never too late for Firefox 88 appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/04/never-too-late-for-firefox-88/


Fri, 16 Apr 2021 20:04:21 | QUIC and HTTP/3 Support now in Firefox Nightly and Beta
Support for QUIC and HTTP/3 is now enabled by default in Firefox Nightly and Firefox Beta and we are planning to start a rollout on the release in Firefox Stable Release 88. HTTP/3 will be available by default by the end of May.
The post QUIC and HTTP/3 Support now in Firefox Nightly and Beta appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/04/quic-and-http-3-support-now-in-firefox-nightly-and-beta/


Tue, 06 Apr 2021 15:21:46 | Eliminating Data Races in Firefox – A Technical Report
We successfully deployed ThreadSanitizer in the Firefox project to eliminate data races in our remaining C/C++ components. In the process, we found several impactful bugs and can safely say that data races are often underestimated in terms of their impact on program correctness. We recommend that all multithreaded C/C++ projects adopt the ThreadSanitizer tool to enhance code quality.
The post Eliminating Data Races in Firefox &#8211; A Technical Report appeared first on Mozilla Hacks - the Web developer blog.

https://hacks.mozilla.org/2021/04/eliminating-data-races-in-firefox-a-technical-report/


